
DROP DATABASE IF EXISTS `pig_task`;

CREATE DATABASE  `pig_task` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

USE `pig_task`;

-- ----------------------------
-- Table structure for data_source
-- ----------------------------
DROP TABLE IF EXISTS `data_source`;
CREATE TABLE `data_source` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) NOT NULL COMMENT '数据源名称',
  `platform` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '所属平台id',
  `url` varchar(255) NOT NULL COMMENT '数据源链接',
  `username` varchar(64) NOT NULL COMMENT '数据源用户名',
  `password` varchar(64) NOT NULL COMMENT '数据源密码',
  `driver_class` varchar(64) NOT NULL COMMENT '数据源驱动类',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='数据源表';

-- ----------------------------
-- Records of data_source
-- ----------------------------

-- ----------------------------
-- Table structure for handler
-- ----------------------------
DROP TABLE IF EXISTS `handler`;
CREATE TABLE `handler` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `handler_name` varchar(255) DEFAULT NULL COMMENT '处理器名称',
  `handler_class` varchar(255) DEFAULT NULL COMMENT '处理器执行类',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='处理器表';

-- ----------------------------
-- Records of handler
-- ----------------------------
INSERT INTO `handler` VALUES ('1', '电表在线检测', 'com.sf.alarm.client.task.impl.AmMonitorTask', '2020-09-08 17:18:29', '2020-09-24 10:25:34');
INSERT INTO `handler` VALUES ('2', '水表在线检测', 'com.sf.alarm.client.task.impl.WaterMonitorTask', '2020-09-08 17:18:29', '2020-09-24 10:25:34');
INSERT INTO `handler` VALUES ('3', '短信发送', 'com.sf.cloud.task.task.job.SmsTask', '2020-09-16 10:31:58', '2020-09-16 10:31:58');
INSERT INTO `handler` VALUES ('4', '水表在线检测（具体到单表）', 'com.sf.alarm.client.task.impl.WaterMeterMonitorTask', '2020-11-11 09:49:57', '2020-11-11 14:40:32');
INSERT INTO `handler` VALUES ('5', '电表在线检测（具体到单表）', 'com.sf.alarm.client.task.impl.AmMeterMonitorTask', '2020-11-11 15:29:55', '2020-11-11 15:29:55');
INSERT INTO `handler` VALUES ('6', '网关在线检测', 'com.sf.alarm.client.task.impl.GatherMonitorTask', '2020-11-11 15:29:55', '2020-11-11 15:29:55');
INSERT INTO `handler` VALUES ('7', '越限报警检测（能源3.0专用）', 'com.sf.alarm.client.task.impl.OutOfLimitTask', '2020-11-11 15:29:55', '2020-11-11 15:29:55');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `project_name` varchar(100) NOT NULL COMMENT '项目',
  `platform` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '平台id',
  `handler_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '处理器',
  `location` varchar(200) NOT NULL COMMENT '报警定位',
  `content` varchar(200) NOT NULL COMMENT '报警内容',
  `state` varchar(10) DEFAULT 'NOT_SEND' COMMENT '任务状态',
  `level` int DEFAULT NULL COMMENT '消息等级(预留)',
  `send_time` datetime DEFAULT NULL COMMENT '发送时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='消息表';
-- ----------------------------
-- Records of message
-- ----------------------------

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `project_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL COMMENT '项目名称',
  `role_id` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='项目分组表';

-- ----------------------------
-- Records of project
-- ----------------------------

-- ----------------------------
-- Table structure for sms_count
-- ----------------------------
DROP TABLE IF EXISTS `sms_count`;
CREATE TABLE `sms_count` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `remain` int NOT NULL COMMENT '剩余短信数量',
  `has_send` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='短信配置表';

-- ----------------------------
-- Records of sms_count
-- ----------------------------
INSERT INTO `sms_count` VALUES ('6', '200', '0');

-- ----------------------------
-- Table structure for task
-- ----------------------------
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(64) NOT NULL COMMENT '任务名称',
  `state` varchar(10) DEFAULT 'STOPPED' COMMENT '任务状态',
  `local_task` tinyint(1) NOT NULL COMMENT '是否本地任务',
  `project_id` int NOT NULL COMMENT '项目id',
  `handler_id` int NOT NULL COMMENT '处理器id',
  `previous_fire_time` datetime DEFAULT NULL COMMENT '上次执行时间',
  `next_fire_time` datetime DEFAULT NULL COMMENT '下次执行时间',
  `cron` varchar(64) NOT NULL COMMENT 'cron表达式',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='任务表';

-- ----------------------------
-- Records of task
-- ----------------------------

-- ----------------------------
-- Table structure for task_data_source
-- ----------------------------
DROP TABLE IF EXISTS `task_data_source`;
CREATE TABLE `task_data_source` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_id` int NOT NULL COMMENT '任务id',
  `data_source_id` int NOT NULL COMMENT '数据源id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='任务数据源表';

-- ----------------------------
-- Records of task_data_source
-- ----------------------------

-- ----------------------------
-- Table structure for filter
-- ----------------------------
DROP TABLE IF EXISTS `filter`;
CREATE TABLE `filter` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `type` varchar(10) DEFAULT NULL COMMENT '过滤器类型',
  `parameter_type` varchar(50) NOT NULL COMMENT '参数类型',
  `parameter` varchar(1500) NOT NULL COMMENT '过滤器',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='过滤器表';

-- ----------------------------
-- Records of filter
-- ----------------------------

-- ----------------------------
-- Table structure for filter_data_source
-- ----------------------------
DROP TABLE IF EXISTS `filter_data_source`;
CREATE TABLE `filter_data_source` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `filter_id` int NOT NULL COMMENT '过滤器id',
  `data_source_id` int NOT NULL COMMENT '数据源id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modified_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='过滤器数据源关系表';

-- ----------------------------
-- Records of filter_data_source
-- ----------------------------

-- ----------------------------
-- Table structure for task_filter
-- ----------------------------
DROP TABLE IF EXISTS `task_filter`;
CREATE TABLE `task_filter` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_id` int NOT NULL COMMENT '任务id',
  `filter_id` int NOT NULL COMMENT '过滤器id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='任务过滤器关系表';

-- ----------------------------
-- Records of task_filter
-- ----------------------------
