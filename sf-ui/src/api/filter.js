import request from '@/router/axios'

export function getfilterDict() {
  return request({
    url: '/task/filter/type/dict',
    method: 'get'
  })
}

export function getfilterParameterTypeDict() {
  console.log("找filter")
  return request({
    url: '/task/filter/parameter/type/dict',
    method: 'get'
  })
}
