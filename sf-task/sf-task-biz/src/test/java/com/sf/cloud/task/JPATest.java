package com.sf.cloud.task;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.date.LocalDateTimeUtil;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import com.sf.cloud.task.task.constant.FilterParameterType;
import com.sf.cloud.task.task.constant.FilterType;
import com.sf.cloud.task.task.constant.MessageState;
import com.sf.cloud.task.task.constant.Platform;
import com.sf.cloud.task.task.dao.DataSourceRepository;
import com.sf.cloud.task.task.dao.MessageRepository;
import com.sf.cloud.task.task.domain.po.*;
import com.sf.cloud.task.task.service.*;
import org.hibernate.annotations.Tuplizer;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
@RunWith(SpringRunner.class)
public class JPATest {

    @Autowired
    private DataSourceRepository dataSourceRepository;

    @Autowired
    private MessageService messageService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private HandlerService handlerService;

    @Autowired
    private TaskService taskService;

	@Autowired
	private MessageRepository messageRepository;

	@Autowired
	private FilterService filterService;

    @Test
    public void testManyToOne() {
        List<DataSource> all = dataSourceRepository.findAll();
        System.out.println(all);
    }

    @Test
    public void testInsertManyToOne() {
        DataSource dataSource = DataSource.builder().name("武大数据源1")
                .platform(Platform.JAVA_ORACLE_ENERGY)
                .username("root")
                .password("root")
                .driverClass("com.driverClass")
                .url("java://mysql")
                .build();
        dataSourceRepository.save(dataSource);
    }

    @Test
    public void findTask() {
        List<Task> tasks = taskService.findAll();
        System.out.println(tasks);
    }

    @Test
    public void findDataSource() {
        List<DataSource> all = dataSourceRepository.findAll();
        System.out.println(all);
    }

	@Test
	public void testFind() {
		Specification<Message> specification = new Specification<Message>() {
			@Override
			public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder cb) {
				//增加筛选条件
				Predicate predicate = cb.conjunction();
				predicate.getExpressions().add(cb.equal(root.get("state"), MessageState.NOT_SEND));
				// 7天以内
				LocalDateTime ago = LocalDateTimeUtil.offset(LocalDateTimeUtil.now(), -7, ChronoUnit.DAYS);
				Date agoDate = Date.from(ago.atZone(ZoneId.systemDefault()).toInstant());
				predicate.getExpressions().add(cb.greaterThanOrEqualTo(root.get("createTime").as(Date.class), agoDate));
				return predicate;
			}
		};
		List<Message> all = messageRepository.findAll(specification);
		System.out.println(all);
	}

	@Test
	public void testDB_Filter() {
//		Filter filter = Filter.builder()
//			.type(FilterType.INCLUDE)
//			.parameterType(FilterParameterType.AM_ADDR)
//			.parameter("123,456")
//			.build();

		Filter filter = filterService.findAll().get(0);
//		DataSource dataSource = dataSourceRepository.findAll().get(0);
//		HashSet<DataSource> dataSources = new HashSet<>();
//		dataSources.add(dataSource);

		filterService.deleteById(filter.getId());
		System.out.println(filter.getId());
	}

	@Test
	public void testTask_Filter() {
		Task task = taskService.findAll().get(0);
		task.getFilters().clear();
		System.out.println(task);
//		Filter filter = Filter.builder()
//			.type(FilterType.INCLUDE)
//			.parameterType(FilterParameterType.AM_ADDR)
//			.parameter("123,456")
//			.build();
//		task.getFilters().add(filter);
		taskService.save(task);
	}

	@Test
	public void testRoleProject() {
		PageRequest pageable = PageRequest.of(0, 2);
		List<Integer> roleIds = Arrays.asList(5);
		List<Project> projects = projectService.findAll();
		List<Project> projectOfRole = projects.stream()
			.sorted((p1, p2) -> (int) (p1.getId() - p2.getId()))
			.filter(p -> roleIds.stream()
				.anyMatch(roleId -> p.getRoleIds().contains(roleId)))
			.collect(Collectors.toList());
		int fromIndex = (int)pageable.getOffset();
		int toIndex = (int) pageable.getOffset() + pageable.getPageSize();
		if (toIndex >= projectOfRole.size()) {
			toIndex = projectOfRole.size();
		}
		List<Project> onePageProjectOfRole = projectOfRole.subList(fromIndex, toIndex);
		PageImpl page = new PageImpl<>(onePageProjectOfRole, pageable, projectOfRole.size());
		System.out.println(page);

	}
}
