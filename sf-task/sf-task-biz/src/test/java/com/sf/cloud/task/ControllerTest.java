package com.sf.cloud.task;

import com.sf.cloud.task.task.controller.MessageController;
import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * @description:
 * @projectName:pig
 * @see:com.sf.cloud.task
 * @author:tzl
 * @createTime:2021/1/4 11:31
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
public class ControllerTest {

	@Autowired
	private MessageController messageController;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		mockMvc = MockMvcBuilders.standaloneSetup(messageController).build();
	}

	/**
	 * 保存加密处理的客户信息
	 * @throws Exception
	 */
	@Test
	public void testClientMessage() throws Exception {
		String msg = "{\"messages\":[{\"handlerName\":\"电表 在线检测\",\"message\":\"(中南财大最近电表离线)位置: 首义校区.中南楼配电房5#.首义中南楼配电房5-1, 4201412700, 离线数量: 1,  总数量: 12。\",\"platform\":\"JAVA_ORACLE_ENERGY\",\"projectName\":\"中南财大\"}],\"taskId\":5}";

		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/message/client")
			.contentType(MediaType.APPLICATION_JSON)
			.content(msg))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andDo(MockMvcResultHandlers.print())
			.andReturn();

		log.info(mvcResult.getResponse().getContentAsString());
	}

}
