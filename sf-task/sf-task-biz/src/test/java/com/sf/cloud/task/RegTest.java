package com.sf.cloud.task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Pattern;

@SpringBootTest
@RunWith(SpringRunner.class)
public class RegTest {

	/**
	 * 处理器com.sf.alarm.client.task.AmMonitorTask
	 * 处理器com.sf.alarm.client.task.WaterMonitorTask
	 *
	  **/
	@Test
	public void testReg1() {
		String contentAm = "(公司平台最近电表离线)位置: 盛帆工业园.dfdfd.1号楼集中器, 4201466500, 离线数量: 81, 总数量: 81。";
		String contentWa = "(公司平台最近水表离线)位置: 盛帆工业园.dfdfd.1号楼集中器, 4201466500, 离线数量: 81, 总数量: 81。";

		String pattern = "^(\\(.*离线\\)位置: .*, 离线数量: )\\d+, (总数量: )\\d+(。)$";

		boolean isMatch = Pattern.matches(pattern, contentAm);
		System.out.println("是否匹配: " + isMatch);

		String result = contentAm.replaceAll(pattern, "$1%$2%$3");
		System.out.println(result);
	}

	/**
	 * 处理器com.sf.alarm.client.task.impl.WaterMeterMonitorTask
	 * 处理器com.sf.alarm.client.task.impl.AmMeterMonitorTask
	 * 处理器com.sf.alarm.client.task.impl.GatherMonitorTask
	 *
	 **/
	@Test
	public void testReg2() {
		String message = "(公司平台水表离线)位置: 盛帆工业园.5号楼.附楼3楼洗手间.001608009559。";
		String pattern = "^(\\(.+。)$";
		boolean isMatch = Pattern.matches(pattern, message);
		System.out.println("是否匹配: " + isMatch);

		String result = message.replaceAll(pattern, "$1");
		System.out.println(result);
	}

	@Test
	public void testReg3() {
		String over = "电表(1) A相电压大于限定值2，当前电压3;B相电压大于限定值4，当前电压5;C相电压大于限定值6，当前电压7;";
		String pattern = "^(电表\\(.*\\) .+?电[压|流][大|小]于).*;$";
		boolean isMatch = Pattern.matches(pattern, over);
		System.out.println("是否匹配: " + isMatch);

		String result = over.replaceAll(pattern, "$1");
		System.out.println(result);
	}
}
