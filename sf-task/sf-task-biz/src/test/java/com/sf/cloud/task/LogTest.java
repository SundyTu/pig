package com.sf.cloud.task;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @description:
 * @projectName:pig
 * @see:com.sf.cloud.task
 * @author:tzl
 * @createTime:2021/3/8 11:25
 */
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class LogTest {

	@Test
	public  void test2(){
		log.debug("debug message");
		log.warn("warn message");
		log.info("info message");
		log.error("error message");
		log.trace("trace message");
	}
}
