package com.sf.cloud.task.task.dao;

import com.sf.cloud.task.task.domain.po.Handler;
import org.springframework.stereotype.Repository;

/**
 * @author tuzhaoliang
 */
@Repository
public interface HandlerRepository extends BaseRepository<Handler,Long> {
	Handler findByHandlerName(String handlerName);
}
