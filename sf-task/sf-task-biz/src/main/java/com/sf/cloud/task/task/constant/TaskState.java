package com.sf.cloud.task.task.constant;

/**
  * @Description 任务状态
  * @Author tuzhaoliang
  * @Date 2020/9/17 8:55
  **/
public enum TaskState {

    /**
      * @Description 运行中
      * @Author tuzhaoliang
      * @Date 2020/9/17 9:09
      **/
    START,
    /**
      * @Description 停止
      * @Author tuzhaoliang
      * @Date 2020/9/17 9:09
      **/
    STOPPED;

}
