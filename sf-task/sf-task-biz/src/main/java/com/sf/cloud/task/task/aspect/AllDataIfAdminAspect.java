package com.sf.cloud.task.task.aspect;

import cn.hutool.core.exceptions.UtilException;
import cn.hutool.core.util.ReflectUtil;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import com.sf.cloud.task.task.constant.RoleId;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.List;
import java.util.stream.Collectors;

/**
  * @Description 使用findAll方法时，如果是管理员角色，则直接返回所有数据
  * @Author tuzhaoliang
  * @Date 2020/11/20 15:13
  **/
@Aspect
@Component
public class AllDataIfAdminAspect {

	/**
	 * findAll 切入点
	 * 匹配com.sf.cloud.task.task.service.impl包下面的所有方法
	 */
	@Pointcut("execution(public * com.sf.cloud.task.task.service.impl..*.findAll())")
	public void serviceImplFindAllPointCut(){}


	/**
	 * findAll环绕通知
	 */
	@Around(value = "serviceImplFindAllPointCut()")
	public Object serviceImplFindAllAround(ProceedingJoinPoint pjp) {
		Object findAllResult;
		try {
			List<Integer> roleIds = SecurityUtils.getRoles();
			if (roleIds.contains(RoleId.ADMIN.getRoleId())) {
				// 是管理员用户操作，则返回所有数据
				Object serviceImpl = pjp.getTarget();
				Method findAllIgnoreRole = serviceImpl.getClass().getMethod("findAllIgnoreRole");
				findAllResult = findAllIgnoreRole.invoke(serviceImpl);
			} else {
				findAllResult = pjp.proceed();
			}

			// 按照id降序排序，这样前端看到最新一条在最上面
			if (findAllResult instanceof List) {
				List findAllResultList = (List) findAllResult;
				final String getIdMethodName = "getId";

				return findAllResultList.stream()
					.sorted((o1, o2) -> {
						try {
							Long id1 = ReflectUtil.invoke(o1, getIdMethodName);
							Long id2 = ReflectUtil.invoke(o2, getIdMethodName);
							return (int) (id2 - id1);
						} catch (UtilException e) {
							return 0;
						}
					}).collect(Collectors.toList());
			}

			return findAllResult;

		} catch (Throwable e) {
			System.out.println(pjp.getSignature() + " 出现异常： " + e);
			return null;
		}
	}

}
