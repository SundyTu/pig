package com.sf.cloud.task.task.constant;

public enum Platform {

    ICBS,
	JAVA_ORACLE_ENERGY,
	JAVA_MYSQL_ENERGY,
    C_ENERGY;
}
