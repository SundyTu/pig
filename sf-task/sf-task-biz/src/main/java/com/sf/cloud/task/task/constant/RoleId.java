package com.sf.cloud.task.task.constant;

import lombok.Getter;

public enum RoleId {
	/**
	  * 管理员
	  **/
	ADMIN(1);

	@Getter
	private int roleId;

	RoleId(int roleId) {
		this.roleId = roleId;
	}
}
