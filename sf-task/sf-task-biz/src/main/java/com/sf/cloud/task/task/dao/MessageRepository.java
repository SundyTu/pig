package com.sf.cloud.task.task.dao;

import com.sf.cloud.task.task.constant.Platform;
import com.sf.cloud.task.task.domain.po.Message;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author tuzhaoliang
 */
@Repository
public interface MessageRepository extends BaseRepository<Message,Long> {

	List<Message> findByProjectNameAndPlatformAndHandlerNameAndLocationOrderByCreateTimeDesc(String projectName, Platform platform, String handlerName, String location);
}
