package com.sf.cloud.task.task.dao;

import com.sf.cloud.task.task.domain.po.Project;
import org.springframework.stereotype.Repository;

/**
  * @Author tuzhaoliang
  * @Date 2020/9/8 17:33
  **/
@Repository
public interface ProjectRepository extends BaseRepository<Project,Long> {
}
