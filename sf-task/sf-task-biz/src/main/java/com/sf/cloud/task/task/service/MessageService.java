package com.sf.cloud.task.task.service;

import com.sf.cloud.task.task.constant.Platform;
import com.sf.cloud.task.task.domain.po.Message;

import java.util.List;

public interface MessageService extends BaseService<Message,Long> {

	/**
	 * @Description 查询7天内未发送的消息
	 * @Author tuzhaoliang
	 * @Date 2020/10/9 9:57
	 **/
	List<Message> findNotSend();

	List<Message> findByProjectNameAndPlatformAndHandlerNameAndLocationOrderByCreateTimeDesc(String projectName, Platform platform, String handlerName, String location);

	/**
	  * @Description 保存上报消息
	  * @Author tuzhaoliang
	  * @Date 2021/3/8 9:22
	  **/
	List<Message> saveClientMessage(Iterable<Message> entities);

}
