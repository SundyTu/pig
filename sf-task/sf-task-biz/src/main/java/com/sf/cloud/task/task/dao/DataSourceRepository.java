package com.sf.cloud.task.task.dao;

import com.sf.cloud.task.task.domain.po.DataSource;
import org.springframework.stereotype.Repository;

/**
  * @Author tuzhaoliang
  * @Date 2020/9/9 14:57
  **/
@Repository
public interface DataSourceRepository extends BaseRepository<DataSource,Long> {
}
