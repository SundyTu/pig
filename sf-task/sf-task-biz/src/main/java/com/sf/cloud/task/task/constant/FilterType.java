package com.sf.cloud.task.task.constant;

/**
  * @Description 过滤器类型
  * @Author tuzhaoliang
  **/
public enum FilterType {
	/**
	  * 包含
	  **/
	INCLUDE("包含"),
	/**
	  * 不包含
	  **/
	EXCLUDE("不包含");

	private String typeName;

	FilterType(String typeName) {
		this.typeName = typeName;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
}
