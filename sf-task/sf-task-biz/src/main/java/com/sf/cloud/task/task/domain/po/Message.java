/*
 *    Copyright (c) 2018-2025, lengleng All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the pig4cloud.com developer nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 * Author: lengleng (wangiegie@gmail.com)
 */

package com.sf.cloud.task.task.domain.po;

import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sf.cloud.task.task.constant.MessageState;
import com.sf.cloud.task.task.constant.Platform;
import com.sf.cloud.task.task.service.HandlerService;
import com.sf.cloud.task.task.utils.SpringBeanUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Tolerate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.regex.Pattern;

/**
 * tuzhaoliang
 *
 * @author tucent
 * @date 2020-09-18 08:31:22
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Builder
@Entity
@Table(name = "message")
@EntityListeners(AuditingEntityListener.class)
@ApiModel(value = "消息表")
public class Message extends BasePo {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@ApiModelProperty(value = "主键")
	private Long id;
	/**
	 * 项目
	 */
	@ApiModelProperty(value = "项目")
	@Column(name = "project_name")
	private String projectName;
	/**
	 * 平台
	 */
	@ApiModelProperty(value = "平台")
	@Column(name = "platform")
	@Enumerated(EnumType.STRING)
	private Platform platform;
	/**
	 * 处理器
	 */
	@ApiModelProperty(value = "处理器")
	@Column(name = "handler_name")
	private String handlerName;
	/**
	 * 报警定位
	 */
	@ApiModelProperty(value = "报警定位")
	@Column
	private String location;
	/**
	 * 报警内容
	 */
	@ApiModelProperty(value = "报警内容")
	@Column
	private String content;
	/**
	 * 任务状态
	 */
	@ApiModelProperty(value = "任务状态")
	@Column
	@Enumerated(EnumType.STRING)
	private MessageState state;
	/**
	 * 消息等级(预留)
	 */
	@ApiModelProperty(value = "消息等级(预留)")
	@Column
	private Integer level;
	/**
	 * 发送时间
	 */
	@ApiModelProperty(value = "发送时间")
	@Column(name = "send_time")
	private LocalDateTime sendTime;
	/**
	 * 创建时间
	 */
	@ApiModelProperty(value = "创建时间")
	@Column(name = "create_time")
	@CreatedDate
	private LocalDateTime createTime;
	/**
	 * 更新时间
	 */
	@ApiModelProperty(value = "更新时间")
	@Column(name = "modified_time")
	@LastModifiedDate
	private LocalDateTime modifiedTime;

	@Tolerate
	public Message() {
	}

	/**
	 * @Description 消息
	 * @Author tuzhaoliang
	 * @Date 2020/9/27 15:15
	 **/
	public String getMessage() {
		String msg = "(" +
			this.getProjectName() +
			")" +
			"位置:" +
			this.getLocation() +
			"," +
			"内容:" +
			this.getContent();
		final String fullStop = "。";
		if (!StrUtil.endWith(msg, fullStop)){
			msg += fullStop;
		}
		return msg;
	}

//	/**
//	 * @Description 消息发生位置
//	 * @Author tuzhaoliang
//	 * @Date 2020/9/27 15:15
//	 **/
//	@JsonIgnore
//	public String getFeature() throws Exception {
//		if (this.getMessage() == null || this.getHandlerName() == null) {
//			throw new Exception("message内容或者handlerName为空");
//		}
//		HandlerService handlerService = SpringBeanUtils.getBean(HandlerService.class);
//		assert handlerService != null;
//		// 每一种处理器产生的消息特征值 都要在这里进行实现提取，用户判断是否有重复消息
//		Handler handler = handlerService.findByHandlerName(this.getHandlerName());
//		if (handler == null || handler.getHandlerClass() == null){
//			throw new Exception("找不到对应handler：" + handler);
//		}
//		String regex;
//		String replacement;
//		switch (handler.getHandlerClass()) {
//			case "com.sf.alarm.client.task.impl.AmMonitorTask" :
//			case "com.sf.alarm.client.task.impl.WaterMonitorTask" :
//				// 这种是报到网关级别的
//				// String content = "(公司平台最近电表离线)位置: 盛帆工业园.dfdfd.1号楼集中器, 4201466500, 离线数量: 81, 总数量: 81。";
//				// String content = "(公司平台最近水表离线)位置: 盛帆工业园.dfdfd.1号楼集中器, 4201466500, 离线数量: 81, 总数量: 81。";
//				regex = "^(\\(.*离线\\)位置: .*, 离线数量: )\\d+, (总数量: )\\d+(。)$";
//				replacement = "$1%$2%$3";
//				break;
//			case "com.sf.alarm.client.task.impl.WaterMeterMonitorTask":
//			case "com.sf.alarm.client.task.impl.AmMeterMonitorTask":
//			case "com.sf.alarm.client.task.impl.GatherMonitorTask":
//				// 这种是报到具体表的，不管网关、电表、水表
//				// (公司平台水表离线)位置: 盛帆工业园.5号楼.附楼3楼洗手间.001608009559。
//				// (公司平台电表离线)位置: 盛帆工业园.5号楼.附楼3楼洗手间.001608009559。
//				// (公司平台网关离线)位置: 盛帆工业园.5号楼.附楼3楼洗手间.001608009559。
//				regex = "^(\\(.+。)$";;
//				replacement = "%$1%";
//				break;
//			case "com.sf.alarm.client.task.impl.OutOfLimitTask":
//				// String over = "电表(1) A相电压大于限定值2，当前电压3;B相电压大于限定值4，当前电压5;C相电压大于限定值6，当前电压7;";
//				regex = "^(电表\\(.*\\) .+?电[压|流][大|小]于).*;$";
//				replacement = "%$1%";
//				break;
//			default:
//				throw new Exception("找不到对应处理器的特征点");
//		}
//		boolean isMatch = Pattern.matches(regex, this.getMessage());
//		if (!isMatch) {
//			throw new Exception("message: " + this.getMessage()
//				+ "与处理器:" + handler.getHandlerClass()
//				+ "不匹配");
//		}
//		return this.getMessage().replaceAll(regex, replacement);
//	}

}
