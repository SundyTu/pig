package com.sf.cloud.task.task.service.impl;

import com.pig4cloud.pig.common.security.service.PigUser;
import com.pig4cloud.pig.common.security.util.SecurityUtils;
import com.sf.cloud.task.task.constant.RoleId;
import com.sf.cloud.task.task.dao.BaseRepository;
import com.sf.cloud.task.task.dao.ProjectRepository;
import com.sf.cloud.task.task.domain.po.Project;
import com.sf.cloud.task.task.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.*;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
  * @Author tuzhaoliang
  * @Date 2020/9/8 17:39
  **/
@RequiredArgsConstructor
@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    @Override
    public BaseRepository<Project, Long> getRepository() {
        return projectRepository;
    }

	@Override
	public Optional<Project> findByName(String projectName) {
		Project project = Project.builder()
			.projectName(projectName)
			.build();
		return getRepository().findOne(Example.of(project));
	}


	@Override
	public List<Project> findAll() {
		List<Integer> roleIds = SecurityUtils.getRoles();
		return this.getRepository().findAll().stream()
					.sorted((p1, p2) -> (int) (p1.getId() - p2.getId()))
					.filter(p -> roleIds.stream()
						.anyMatch(roleId -> p.getRoleIds().contains(roleId)))
					.collect(Collectors.toList());
	}

}
