package com.sf.cloud.task.task.service;

import com.sf.cloud.task.task.dao.BaseRepository;
import com.sf.cloud.task.task.domain.po.Project;
import com.sf.cloud.task.task.utils.SpringBeanUtils;
import org.springframework.aop.framework.AopContext;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
  * @Description
  * @Author tuzhaoliang
  * @Date 2020/7/6 8:28
  **/
@FunctionalInterface
public interface BaseService<T,ID> {

    BaseRepository<T, ID> getRepository();

    default long count() {
        return getRepository().count();
    }

    default T findById(ID id) {
		return getRepository().findById(id).orElse(null);
    }

    default List<T> findAll(){
        return getRepository().findAll();
    }

    /**
      * @Description 默认根据findAll查出来的数据进行角色筛选分页
      * @Author tuzhaoliang
      * @Date 2020/11/20 13:36
      **/
    default Page<T> findAll(Pageable pageable){
    	// 这种写法是由于内部直接调用findAll会导致aop拦截失败问题（SpringBoot AOP无法拦截类内部的调用方法）
		// 具体查看 https://blog.csdn.net/qq_43047801/article/details/89704743
		BaseService baseService = SpringBeanUtils.getBean(this.getClass());
		List tOfRole = baseService.findAll();
		int fromIndex = (int)pageable.getOffset();
		int toIndex = (int) pageable.getOffset() + pageable.getPageSize();
		if (toIndex >= tOfRole.size()) {
			toIndex = tOfRole.size();
		}
		List<T> onepageTofrole = tOfRole.subList(fromIndex, toIndex);
		return new PageImpl<>(onepageTofrole, pageable, tOfRole.size());
    }

	default List<T> findAllIgnoreRole(){
		return getRepository().findAll();
	}

	default Page<T> findAllIgnoreRole(Pageable pageable){
		return getRepository().findAll(pageable);
	}


    default <S extends T> Page<S> findAll(S s, Pageable pageable){
        Example<S> example = Example.of(s);
        return getRepository().findAll(example, pageable);
    }

    default <S extends T> Page<S> findAll(Example<S> s, Pageable pageable){
        return getRepository().findAll(s, pageable);
    }

    default <S extends T> S findOne(Example<S> example){
        return getRepository().findOne(example).orElse(null);
    }

    default T save(T t) {
        return getRepository().save(t);
    }

    default <S extends T> List<S> saveAll(Iterable<S> entities){
        return getRepository().saveAll(entities);
    }

    default boolean deleteById(ID id) {
        try {
            getRepository().deleteById(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    default boolean delete(T t) {
        try {
            getRepository().delete(t);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
