package com.sf.cloud.task.task.service.impl;

import com.sf.cloud.task.task.dao.BaseRepository;
import com.sf.cloud.task.task.dao.FilterRepository;
import com.sf.cloud.task.task.domain.po.Filter;
import com.sf.cloud.task.task.service.FilterService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * @Author tuzhaoliang
 **/
@Slf4j
@RequiredArgsConstructor
@Service
public class FilterServiceImpl implements FilterService {

	private final FilterRepository filterRepository;

	@Override
	public BaseRepository<Filter, Long> getRepository() {
		return filterRepository;
	}

	@Override
	public boolean delete(Filter filter) {
		try {
			this.deleteById(filter.getId());
			filterRepository.deleteFilterDataSource(filter.getId());
			return true;
		} catch (Exception e) {
			log.error("删除filter出错", e);
		}
		return false;
	}
}
