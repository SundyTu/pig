package com.sf.cloud.task.task.constant;

public enum MessageState {
    /**
      * @Description 未发送
      * @Author tuzhaoliang
      * @Date 2020/9/18 8:41
      **/
    NOT_SEND,
    /**
      * @Description 已发送
      * @Author tuzhaoliang
      * @Date 2020/9/18 8:41
      **/
    HAS_SEND;
}
