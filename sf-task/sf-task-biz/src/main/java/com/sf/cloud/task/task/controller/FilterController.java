package com.sf.cloud.task.task.controller;

import cn.hutool.core.map.MapUtil;
import com.pig4cloud.pig.common.core.util.R;
import com.sf.cloud.task.task.constant.FilterParameterType;
import com.sf.cloud.task.task.constant.FilterType;
import com.sf.cloud.task.task.domain.po.DataSource;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 过滤器api
 *
 * @author tucent
 * @date 2020-09-09 11:18:30
 */
@RestController
@RequiredArgsConstructor
@RequestMapping("/filter" )
@Api(value = "filter", tags = "过滤器")
public class FilterController {

	/**
	 * 过滤器的类型
	 * @return
	 */
	@ApiOperation(value = "过滤器的类型", notes = "过滤器的类型")
	@GetMapping("/type/dict" )
	public R getFilterType() {
		FilterType[] filterTypes = FilterType.values();
		List<HashMap<String, String>> options = Arrays.stream(filterTypes)
			.map(filterType -> {
				HashMap<String, String> option = MapUtil.newHashMap();
				option.put("label", filterType.getTypeName());
				option.put("value", filterType.toString());
				return option;
			}).collect(Collectors.toList());
		return R.ok(options);
	}

	/**
	 * 过滤器的参数类型
	 * @return
	 */
	@ApiOperation(value = "过滤器的参数类型", notes = "过滤器的参数类型")
	@GetMapping("/parameter/type/dict" )
	public R getFilterParameterType() {
		FilterParameterType[] filterParameterTypes = FilterParameterType.values();
		List<HashMap<String, String>> options = Arrays.stream(filterParameterTypes)
			.map(filterType -> {
				HashMap<String, String> option = MapUtil.newHashMap();
				option.put("label", filterType.getParamType());
				option.put("value", filterType.toString());
				return option;
			}).collect(Collectors.toList());
		return R.ok(options);
	}
}
