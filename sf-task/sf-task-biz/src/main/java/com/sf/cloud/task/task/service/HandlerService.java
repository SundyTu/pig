package com.sf.cloud.task.task.service;

import com.sf.cloud.task.task.domain.po.Handler;

public interface HandlerService extends BaseService<Handler,Long> {

	Handler findByHandlerName(String handlerName);
}
