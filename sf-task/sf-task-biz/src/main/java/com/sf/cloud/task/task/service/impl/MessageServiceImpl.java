package com.sf.cloud.task.task.service.impl;

import cn.hutool.core.date.LocalDateTimeUtil;
import com.sf.cloud.task.task.constant.MessageState;
import com.sf.cloud.task.task.constant.Platform;
import com.sf.cloud.task.task.dao.BaseRepository;
import com.sf.cloud.task.task.dao.MessageRepository;
import com.sf.cloud.task.task.domain.po.Message;
import com.sf.cloud.task.task.domain.po.Project;
import com.sf.cloud.task.task.service.MessageService;
import com.sf.cloud.task.task.service.ProjectService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @Description 消息服务类
 * @Author tuzhaoliang
 * @Date 2020/9/18 8:46
 **/
@Slf4j
@RequiredArgsConstructor
@Service
public class MessageServiceImpl implements MessageService {

	private final MessageRepository messageRepository;

	private final ProjectService projectService;

	@Override
	public BaseRepository<Message, Long> getRepository() {
		return messageRepository;
	}

	@Override
	public Message save(Message message) {
		if (message.getState() == null) {
			message.setState(MessageState.NOT_SEND);
		}
		return getRepository().save(message);
	}

	/**
	 * @param entities
	 * @Description 保存上报消息
	 * @Author tuzhaoliang
	 * @Date 2021/3/8 9:22
	 */
	@Override
	public List<Message> saveClientMessage(Iterable<Message> entities) {
		List<Message> needToSaveMessage = new ArrayList<>();
		for (Message message : entities) {
			boolean isNeedToSave = this.clientMsgNeedToSave(message);
			if (!isNeedToSave){
				continue;
			}

			if (message.getState() == null) {
				message.setState(MessageState.NOT_SEND);
			}
			needToSaveMessage.add(message);
		}

		return getRepository().saveAll(needToSaveMessage);
	}

	/**
	  * @Description 客户端上报消息判断是否需要入库
	  * @Return
	  * @Author tuzhaoliang
	  * @Date 2021/3/8 9:02
	  **/
	private boolean clientMsgNeedToSave(Message message) {
		log.info("开始判断消息是否应该存入");
		log.info(message.getMessage());
		List<Message> similarMessagePage =
			this.findByProjectNameAndPlatformAndHandlerNameAndLocationOrderByCreateTimeDesc(
				  message.getProjectName()
				, message.getPlatform()
				, message.getHandlerName()
				, message.getLocation());
		log.info("数据库有相同消息 {} 条", similarMessagePage.size());
		if (similarMessagePage.isEmpty()) {
			// 没有相似的消息 直接存入
			log.info("没有相似的消息 允许入库");
			return true;
		}
		// 有相似的消息，判断时间 决定是否存入
		Message similarMessage = similarMessagePage.get(0);
		log.debug("最近一条相似消息为: {}", similarMessage);
		Duration between = LocalDateTimeUtil.between(similarMessage.getCreateTime()
			, LocalDateTimeUtil.now());
		long betweenDays = between.toDays();
		log.info("与相似消息相隔 {} 天", betweenDays);
		boolean need = betweenDays > 7;
		log.info("是否允许入库: {}", need);
		return need;
	}

	@Override
	public List<Message> findNotSend() {
		Specification<Message> specification = new Specification<Message>() {
			@Override
			public Predicate toPredicate(Root root, CriteriaQuery query, CriteriaBuilder cb) {
				//增加筛选条件
				Predicate predicate = cb.conjunction();
				predicate.getExpressions().add(cb.equal(root.get("state"), MessageState.NOT_SEND));
				// 7天以内
				LocalDateTime ago = LocalDateTimeUtil.offset(LocalDateTimeUtil.now(), -7, ChronoUnit.DAYS);
				Date agoDate = Date.from(ago.atZone(ZoneId.systemDefault()).toInstant());
				predicate.getExpressions().add(cb.greaterThanOrEqualTo(root.get("createTime").as(Date.class), agoDate));
				return predicate;
			}
		};
		return messageRepository.findAll(specification);
	}

	@Override
	public List<Message> findByProjectNameAndPlatformAndHandlerNameAndLocationOrderByCreateTimeDesc(
		String projectName, Platform platform, String handlerName, String location) {
		return this.messageRepository
			.findByProjectNameAndPlatformAndHandlerNameAndLocationOrderByCreateTimeDesc(
				projectName, platform, handlerName, location);
	}

	private Specification<Message> getThisRoleSpecification() {
		// 查询当前用户 能获取到的project
		List<Project> thisRoleProjects = projectService.findAll();
		return new Specification<Message>() {
			@Override
			public Predicate toPredicate(Root<Message> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				//增加筛选条件
				List<Predicate> predicateList = new ArrayList<Predicate>();
				for (Project project : thisRoleProjects) {
					Predicate predicate = criteriaBuilder.equal(root.get("projectName"), project.getProjectName());
					predicateList.add(predicate);
				}
				return criteriaBuilder.or(predicateList.toArray(new Predicate[0]));
			}
		};
	}

	@Override
	public List<Message> findAll() {
		Specification<Message> specification = this.getThisRoleSpecification();
		return this.getRepository().findAll(specification);
	}

	/**
	 * @param pageable
	 * @Description 默认根据findAll查出来的数据进行角色筛选分页
	 * @Author tuzhaoliang
	 * @Date 2020/11/20 13:36
	 */
	@Override
	public Page<Message> findAll(Pageable pageable) {
		Specification<Message> specification = this.getThisRoleSpecification();
		return this.getRepository().findAll(specification, pageable);
	}
}
