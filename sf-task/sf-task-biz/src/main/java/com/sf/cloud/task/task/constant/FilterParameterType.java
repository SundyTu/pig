package com.sf.cloud.task.task.constant;

/**
  * @Description 过滤器参数类型
  * @Author tuzhaoliang
  * @Date 2020/10/29 10:08
  **/
public enum FilterParameterType {
	/**
	  * 电表地址
	  **/
	AM_ADDR("电表地址"),
	/**
	 * 水表地址
	 **/
	WA_ADDR("水表地址"),
	/**
	 * 网关地址
	 **/
	GATHER_ADDR("网关地址");


	private String paramType;

	FilterParameterType(String paramType) {
		this.paramType = paramType;
	}

	public String getParamType() {
		return paramType;
	}

	public void setParamType(String paramType) {
		this.paramType = paramType;
	}
}
