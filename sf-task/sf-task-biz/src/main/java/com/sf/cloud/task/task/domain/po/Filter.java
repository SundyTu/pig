package com.sf.cloud.task.task.domain.po;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sf.cloud.task.task.constant.FilterParameterType;
import com.sf.cloud.task.task.constant.FilterType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Tolerate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 过滤器表
 *
 * @author tucent
 * @date 2020-09-09 11:18:30
 */
@Setter
@Getter
@Builder
@Entity
@Table(name = "filter")
@EntityListeners(AuditingEntityListener.class)
@ApiModel(value = "过滤器表")
public class Filter extends BasePo {
	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@ApiModelProperty(value = "主键")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	/**
	 * 过滤器类型
	 */
	@ApiModelProperty(value = "过滤器类型")
	@Column
	@Enumerated(EnumType.STRING)
	private FilterType type;

	/**
	 * 过滤器参数类型
	 */
	@ApiModelProperty(value = "过滤器参数类型")
	@Column(name = "parameter_type")
	@Enumerated(EnumType.STRING)
	private FilterParameterType parameterType;

	@ApiModelProperty(value = "参数")
	@Column
	private String parameter;

	/**
	 * 过滤器
	 **/
	@Builder.Default
	@ApiModelProperty(value = "过滤器")
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "filter_data_source",joinColumns = @JoinColumn(name = "filter_id", referencedColumnName = "id"),
		inverseJoinColumns = @JoinColumn(name = "data_source_id", referencedColumnName = "id"))
	private Set<DataSource> dataSources = new HashSet<>();


	@JsonIgnore
	@ManyToMany(mappedBy = "filters",fetch=FetchType.LAZY)
	private List<Task> tasks;

	@Tolerate
	public Filter() {}
}
