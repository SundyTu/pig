package com.sf.cloud.task.task.service;

import com.sf.cloud.task.task.domain.po.Filter;

public interface FilterService extends BaseService<Filter, Long> {
}
