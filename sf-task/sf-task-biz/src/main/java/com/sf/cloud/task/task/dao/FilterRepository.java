package com.sf.cloud.task.task.dao;

import com.sf.cloud.task.task.domain.po.Filter;
import org.apache.ibatis.annotations.Update;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

/**
  * @Author tuzhaoliang
  * @Date 2020/10/30 9:38
  **/
@Repository
public interface FilterRepository extends BaseRepository<Filter, Long> {

	@Transactional
	@Modifying
	@Query(value = "delete from filter_data_source where filter_id = :filterId",nativeQuery = true)
	void deleteFilterDataSource(@Param("filterId") Long filterId);

	/**
	 * Deletes a given entity.
	 *
	 * @param filter must not be {@literal null}.
	 * @throws IllegalArgumentException in case the given entity is {@literal null}.
	 */

	@Override
	default void delete(Filter filter){
		this.deleteById(filter.getId());
	}

	@Transactional
	@Modifying
	@Query(value = "delete from filter where id = :id",nativeQuery = true)
	@Override
	void deleteById(@Param("id") Long filterId);
}
