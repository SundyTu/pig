package com.sf.cloud.task.task.service;

import com.sf.cloud.task.task.domain.po.DataSource;

import java.util.Optional;

/**
  * @Author tuzhaoliang
  * @Date 2020/9/9 14:58
  **/
public interface DataSourceService extends BaseService<DataSource,Long> {

	/**
	  * @Description id转换为数据库对象
	  * @Author tuzhaoliang
	  * @Date 2020/11/2 13:44
	  **/
	Optional<DataSource> idToDomain(Long dataSourceId);
}
